<?php

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Artisan;

use App\Events\UpdateComing;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\Models\Runner;
use App\Models\Mission;
use App\Models\MissionResult;
use App\Models\ConditionResult;
use App\Models\Condition;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});




Route::get('/migrate', function () {
        Artisan::call('migrate:fresh');
        return 'migrated';
});

Route::get('/seed', function () {
        Artisan::call('db:seed');
        return 'seeded';
});

Route::get('/rd', function () {
     	$updates = Telegram::getUpdates();
        dd($updates);
});




Route::get('/get', function () {
    foreach ($runners as $key => $runner) {



                Telegram::sendMessage([     
                    'chat_id' =>  $runner->chat_id,
                    'text' => $text,
                    'parse_mode' => 'Markdown'
                 ]);
    }
});



Route::get('update/{id}', function ($day) {
    $runners = Runner::all();
    $mission = Mission::where('day_id', $day)->first();

    foreach ($runners as $key => $runner) {
        if($runner->state->state == 3){
            $first = 0;
            $cfirst = 0;

            $mission_result = MissionResult::create([
                'runner_id' => $runner->id,
                'mission_id' => $mission->id,
            ]);
            foreach ($mission->conditions as $key => $condition) {
                $condition_result = ConditionResult::create([
                    'mission_result_id' => $mission_result->id,
                    'condition_id' => $condition->id,
                ]);
                if($key == 0){
                    $first = $condition_result->id;
                    $cfirst = $condition->id;
                }
            }
            }
            $runner->state->update([
                'mission_id' => $mission->id,
                'condition_id' => $cfirst,
                'condition_result_id' => $first,
                'state' => 0,
            ]);

        } 

});








Route::get('/set', function () {
   $response = Telegram::setWebhook(['url' => 'https://marathon.salesrebels.pro/<token>/webhook']);

    dd($response);
});

Route::post('/<token>/webhook', function () {
    $update = Telegram::getWebhookUpdates();
    event(new UpdateComing($update) );
    return 'ok';
});



Route::get('file/{id}', function ($id) {
    dd(Telegram::getFile(['file_id' => $id]));
});

