<?php
namespace App\Services;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\Models\Runner;
use App\Models\RunnerState;
use App\Models\RunnerProfile;
use App\Models\Mission;
use App\Models\Condition;
use App\Models\MissionResult;
use App\Models\ConditionResult;
use Log;
use App\Services\TestService;
use App\Services\OpenService;
  
class OpenService
{

    public $cr;
    public $mr;
    public $update;
    public $runner;

	public function handle($mr, $update, $runner)
    {

        $this->mr = $mr;
       /* $cr = $cr;*/
        $this->update = $update;
        $this->runner = $runner;

    	$this->openCondition();
    }

    public function openCondition(){

        foreach ($this->mr->condition_results as $cr) {
            if($cr->status_id == 0){
                $this->sendAnswer($cr);
                break;
            }
        }
   }

   public function sendAnswer($cr)
   {


        $text = $cr->condition->start_text;

        if( $cr->condition->type_id == 1 || $cr->condition->type_id == 5){
            $cr->update([
                'status_id' => 1,
            ]);
            $reply_markup = Telegram::replyKeyboardHide();  
        } elseif( $cr->condition->type_id == 2){
            
            $keyboard = [ ['Начать тест'] ];
            
            $reply_markup = Telegram::replyKeyboardMarkup([ 
                'keyboard' => $keyboard, 
                'resize_keyboard' => true,
                'one_time_keyboard' => false,
            ]);
        }elseif( $cr->condition->type_id == 3){    
            $cr->update([
                'status_id' => 1,
            ]);        
            $reply_markup = Telegram::replyKeyboardHide();  
        }elseif( $cr->condition->type_id == 4){
            
            $keyboard = [ ['Отправить код'] ];
            
            $reply_markup = Telegram::replyKeyboardMarkup([ 
                'keyboard' => $keyboard, 
                'resize_keyboard' => true,
                'one_time_keyboard' => false,
            ]);
        }


        if( isset( $this->update['message'] ) ){
            $chat_id =  $this->update['message']['chat']['id'];       
        }            

        if( isset( $this->update['callback_query'] ) ){
            $chat_id = $this->update['callback_query']['from']['id'];        
        }

        
        if($cr->condition->files){
            foreach ($cr->condition->files as $key => $file) {
                if($file->position == 0){
                    $this->sendFile($file, $chat_id);
                }
            }
        }


        Telegram::sendMessage([     
            'chat_id' => $chat_id,
            'text' => $text,
            'parse_mode' => 'Html',
            'reply_markup' => $reply_markup,
        ]);


        
        if($cr->condition->files){
            foreach ($cr->condition->files as $key => $file) {
                if($file->position == 1){
                    $this->sendFile($file, $chat_id);
                }
            }
        }

        $this->runner->state->update([
            'condition_id' => $cr->condition_id,
            'condition_result_id' => $cr->id,
            'state' => 0,
        ]);        



   }


   public function sendFile($file, $chat_id){
        $params = [
            'chat_id' => $chat_id,
        ];

        if($file->type == 0){
            $params['photo'] = $file->file_id;
            Telegram::sendPhoto($params);
        }                       
        
        if($file->type == 1){
            $params['document'] = $file->file_id;
            Telegram::sendDocument($params);
        }
	if($file->type == 2){
		$params['video'] = $file->file_id;
		Telegram::sendVideo($params);
	}  
   }


   

}
