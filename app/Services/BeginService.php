<?php
namespace App\Services;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\Models\Runner;
use App\Models\RunnerState;
use App\Models\RunnerProfile;
use App\Models\Mission;
use App\Models\Condition;
use App\Models\MissionResult;
use App\Models\ConditionResult;

use App\Services\OpenService;

use Log;

  
class BeginService
{
	public $update;
	public $runner;
	public $mission;
	public $condition;
	public $mr;
	public $cr;


	public function handle($data)
    {
    	$this->update = $data['update'];
    	$this->runner = $data['runner'];
    	$this->mission = isset( $data['mission'] ) ? $data['mission'] : null;
    	$this->condition = isset( $data['condition'] ) ? $data['condition'] : null;
    	$this->mr = isset( $data['mr'] ) ? $data['mr'] : MissionResult::where('runner_id', $this->runner->id)->where('mission_id', $this->runner->state->mission_id)->first();
    	$this->cr = isset( $data['cr'] ) ? $data['cr'] :ConditionResult::where('id', $this->runner->condition_result_id)->first();

        $this->handleState();
        //$this->composeAnswer();

    }

    public function handleState()
    {
        if($this->cr->status_id == 0){
            $os = new OpenService;
            $os->handle( $this->mr, $this->update, $this->runner );
        }
    }

 
}
