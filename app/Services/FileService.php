<?php
namespace App\Services;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\Models\Runner;
use App\Models\RunnerState;
use App\Models\RunnerProfile;
use App\Models\Mission;
use App\Models\Condition;
use App\Models\MissionResult;
use App\Models\ConditionResult;
use Log;
use App\Services\TestService;
use App\Services\OpenService;
use App\Services\CloseService;
  
class FileService
{
    public $update;
    public $runner;
    public $mission;
    public $condition;
    public $mr;
    public $cr;
    public $file;

	public function handle($data)
    {

        $this->update = $data['update'];
        $this->runner = $data['runner'];
        $this->mission = $data['mission'];
        $this->condition  = $data['condition'];
        $this->mr  = $data['mr'];
        $this->cr  = $data['cr'];

    	$this->handleState();
    }



    public function handleState()
    {
        if(isset( $this->update['message']['photo'] ) ){
            $this->files = $this->update['message']['photo'];
            $this->getFile();
        } else {
            $this->noFile();
        }
    }

    public function getFile(){
        foreach ($this->update['message']['photo'] as $key => $f) {
            $file = Telegram::getFile( ['file_id' => $f['file_id'] ]);
            $this->cr->result = $this->cr->result.';'.$file['file_path'];
            $this->cr->save();
        }
        $this->cr->files_count += 1;
        $this->cr->save();

        if($this->cr->files_count == $this->condition->files_total ){
            $close = new CloseService;
            $close->handle($this->cr, $this->update);  
        } else {
            $r = $this->cr->condition->files_total - $this->cr->files_count;
            $text = 'Вы прислали '.$this->cr->files_count.' файлов, осталось еще '.$r;
            Telegram::sendMessage([     
                'chat_id' => $this->update['message']['chat']['id'],
                'text' => $text,
                'parse_mode' => 'Html'
            ]); 
        }
    }



    public function noFile(){
        Telegram::sendMessage([     
            'chat_id' => $this->update['message']['chat']['id'],
            'text' => 'отправьте файл',
            'parse_mode' => 'Html'
        ]);         
    }



}