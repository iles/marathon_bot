<?php
namespace App\Services;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\Models\Runner;
use App\Models\RunnerState;
use App\Models\RunnerProfile;
use App\Models\Mission;
use App\Models\Condition;
use App\Models\MissionResult;
use App\Models\ConditionResult;
use Log;
use App\Services\TestService;
use App\Services\CloseService;

  
class CodeService
{
    public $update;
    public $runner;
    public $mission;
    public $condition;
    public $mr;
    public $cr;

	public function handle($data)
    {

        $this->update = $data['update'];
        $this->runner = $data['runner'];
        $this->mission = isset( $data['mission'] ) ? $data['mission'] : null;
        $this->condition = isset( $data['condition'] ) ? $data['condition'] : null;
        $this->mr = isset( $data['mr'] ) ? $data['mr'] : null;
        $this->cr = isset( $data['cr'] ) ? $data['cr'] : null;

        if ( isset($this->update['callback_query']) ){            
            $cdData = json_decode($this->update['callback_query']['data'], true);
            if( $cdData['type'] == 'code_confirm' ){
               $this->handleCallback( $cdData['data'] );
            }
        } else {
    	   $this->handleState();            
        }

    }



    public function handleState()
    {
            if( isset( $this->update['message'] ) ){            
                $chat_id =  $this->update['message']['chat']['id'];

                $code = $this->update['message']['text'];
                $text = 'Вы отправили код: '.$code;

                $url_arr[0]['text']='Подтвердить'; 
                $url_arr[0]['callback_data']='{"type" : "code_confirm", "data": "1"}';          

                $url_arr[1]['text']='Отклонить'; 
                $url_arr[1]['callback_data']='{"type": "code_confirm", "data": "2"}'; 
           
                $this->cr->result = $code;
                $this->cr->save();

                Telegram::sendMessage([     
                    'chat_id' => $chat_id,
                    'text' => $text,
                    'parse_mode' => 'Html',
                    'reply_markup' =>  json_encode(['inline_keyboard'=>array($url_arr)])  ,
                ]);


            }            

    }                  


    public function handleCallback($id){

        if($id == 1){
            if( $this->cr->result == $this->cr->code){                
                $this->cr->status_id = 2;
                $this->cr->save();

                    
                $this->runner->state->state = 3;
                $this->runner->state->save();

                Telegram::sendMessage([     
                    'chat_id' => $this->update['callback_query']['from']['id'],
                    'text' => $this->cr->end_text,
                    'parse_mode' => 'Html',
                    'reply_markup' => Telegram::replyKeyboardHide(),
                ]);
            } else {
                $this->runner->profile->lifes = $this->runner->profile->lifes - 1;
                $this->runner->profile->save();

                $keyboard = [ ['Отправить код'] ];
            
                $reply_markup = Telegram::replyKeyboardMarkup([ 
                    'keyboard' => $keyboard, 
                    'resize_keyboard' => true,
                    'one_time_keyboard' => false,
                ]);

                Telegram::sendMessage([     
                    'chat_id' => $this->update['callback_query']['from']['id'],
                    'text' => 'Вы отправили неверный код, у вас сгорела одна жизнь',
                    'parse_mode' => 'Html',
                    'reply_markup' => $reply_markup,
                ]);
            }            
        } else {
                $keyboard = [ ['Отправить код'] ];
            
                $reply_markup = Telegram::replyKeyboardMarkup([ 
                    'keyboard' => $keyboard, 
                    'resize_keyboard' => true,
                    'one_time_keyboard' => false,
                ]);

                $this->cr->result = null;
                $this->cr->save();

                $this->runner->state->state = 0;
                $this->runner->state->save();

                Telegram::sendMessage([     
                    'chat_id' => $this->update['callback_query']['from']['id'],
                    'text' => 'Код сброшен!',
                    'parse_mode' => 'Html',
                    'reply_markup' => $reply_markup,
                ]);           
        }
    }






}