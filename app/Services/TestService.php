<?php
namespace App\Services;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\Models\Runner;
use App\Models\RunnerState;
use App\Models\RunnerProfile;
use App\Models\Mission;
use App\Models\Condition;
use App\Models\MissionResult;
use App\Models\ConditionResult;
use App\Models\Tests;
use App\Services\OpenService;

use Log;
  
class TestService
{
    public $update;
    public $runner;
    public $mission;
    public $condition;
    public $mr;
    public $cr;
    public $test;
	
	public function handle($data)
	{
        $this->update = $data['update'];
        $this->runner = $data['runner'];
        $this->mission = $data['mission'];
        $this->condition  = $data['condition'];
        $this->mr  = $data['mr'];
        $this->cr  = $data['cr'];




        if ( isset($this->update['callback_query']) ){            
            $cdData = json_decode($this->update['callback_query']['data'], true);
            if( $cdData['type'] == 'test_results_confirm' ){
               $this->handleCallback( $cdData['data'] );
            }
        } else {
            $this->proceed();   
        }

    }

    public function proceed()
    {
		$test = Tests::where('condition_id', $this->cr->condition->id)->first();
		$this->test = json_decode( $test->body, true );
        $state = $this->runner->state;
		if( isset($this->update['message']['text']) && $this->cr->status_id == 1 ){
        	$this->proceedAnswer();
        }

		if($this->cr->test_status == 0){
            $this->cr->update([
                'test_status' => 1,
                'status_id' => 1,
            ]);
			$this->send(1);
    	}
	}

	public function proceedAnswer()
	{
		$update = $this->update;
		if( $update['message']['text'] == 'А' || $update['message']['text'] == 'Б' || $update['message']['text'] == 'В' ){
			if($this->cr->result == null){
	            $answer[1] = $update['message']['text'];
	            $this->cr->result  = json_encode($answer, JSON_UNESCAPED_UNICODE);
            	$this->cr->save();
            	$this->send(2);
        	} else {
        		$test_answers = json_decode($this->cr->result, true);

        		if( $this->cr->test_status <= count($this->test) + 1 ){
	        		array_push($test_answers,  $update['message']['text'] );
	                $this->cr->result = json_encode($test_answers, JSON_UNESCAPED_UNICODE);
	        		$n = count($test_answers);
	        		$this->cr->save();
					$this->send($this->cr->test_status);   			
        		}else{
        			$this->finishTest();
					return false;
        		}
        	}
        } else {				
				$keyboard = [ ['А', 'Б', 'В'] ];
				$text = 'Выберите вариант ответа';
				
				$reply_markup = Telegram::replyKeyboardMarkup([ 
	                'keyboard' => $keyboard, 
	                'resize_keyboard' => true,
	                'one_time_keyboard' => false,
	            ]);

	            Telegram::sendMessage([     
	            	'chat_id' => $update['message']['chat']['id'],
	                'text' => $text,
	                'reply_markup' => $reply_markup,
	                'parse_mode' => 'Markdown'
	             ]);
        	}
	}

	public function send($number = 1){

			if( $number >count($this->test) ){
				$this->finishTest();
				return false;
			}

            $q = $this->test[$number]["q"].'
';
            $a = $this->test[$number]["a"];
            $ans = $q;
            foreach ($a as $key => $value) {
            	$ans = $ans.$key.': '.$value.'
';
            }

            $this->cr->test_status = $number + 1;
            $this->cr->save();

		    $keyboard = [ ['А', 'Б', 'В'] ];
            $reply_markup = Telegram::replyKeyboardMarkup([ 
                'keyboard' => $keyboard, 
                'resize_keyboard' => true,
                'one_time_keyboard' => false,
            ]);

            Telegram::sendMessage([     
                'chat_id' => $this->update['message']['chat']['id'],
                'text' => $ans,
                'reply_markup' => $reply_markup,
                'parse_mode' => 'markdown'
            ]);
	}

	public function finishTest()
	{
        $points = $this->countPoints(); 

        $text = 'Вы набрали '.$points.' очков';        

        Telegram::sendMessage([     
            'chat_id' => $this->update['message']['chat']['id'],
            'text' => $text,
            'reply_markup' => Telegram::replyKeyboardHide(),
            'parse_mode' => 'Markdown'
        ]);

		$url_arr[0]['text']='Пройти тест заново';
		$url_arr[0]['callback_data']='{"type" : "test_results_confirm", "data": "1"}';    
		
		$url_arr[1]['text']='Показать ответы';
        $url_arr[1]['callback_data']='{"type": "test_results_confirm", "data": "2"}';

        $text2 = 'Пройти тест заново или показать результаты';

        Telegram::sendMessage([     
            'chat_id' => $this->update['message']['chat']['id'],
            'text' => $text2,
            'reply_markup'=> json_encode(['inline_keyboard'=>array($url_arr)]),
            'parse_mode' => 'Markdown'
        ]);
	}

	public function countPoints()
    {
        $answers = json_decode($this->cr->result, true);
        if(!$answers){
            return false;
        }
        $points = 0;
        $test = Tests::where('condition_id', $this->cr->condition_id)->first();
        $right_answers = json_decode($test->answers, true);
        $m = count($right_answers);
        foreach ($answers as $key => $value) {
            if($key >= $m +1){
                break;
            }
           if( $value == $right_answers[$key] ){
                $points ++;
           }else {

           }
        }
        return $points;
    }


    public function handleCallback($id)
    {
    	if($id == 1){
    		$this->renewTest();
    	} elseif($id == 2) {
    		$this->closeTest();    		
    	}
    }

    public function newConditionStart($c){

        if( $c->condition->start_text ){
            if( $this->update['callback_query'] ){
                $mid = $this->update['callback_query']['message']['chat']['id'];
            } else {
                $mid = $this->update['message']['chat']['id'];
            }
            Telegram::sendMessage([     
                    'chat_id' => $mid,
                    'text' => $c->condition->start_text,
                    'parse_mode' => 'Markdown',
            ]);           
        }


        if($this->cr->condition->files){
            $params = [
                'chat_id' => $this->update['message']['chat']['id'],
            ];
            foreach ($this->cr->condition->files as $key => $file) {
                if($file->position == 0){
                        if($file->type == 0){
                            $params['photo'] = $file->file_id;
                            Telegram::sendPhoto($params);
                        }                       
                        if($file->type == 1){
                            $params['document'] = $file->file_id;
                            Telegram::sendDocument($params);
                        }                 
                }
            }
        }

        $this->runner->state->condition_id = $c->condition->id;
        $this->runner->state->condition_result_id = $c->id;

        $this->runner->state->state = 0;
        $this->runner->state->save();

        if($c->condition->type_id == 3){

            $keyboard = [ ['Отправить файл'] ];
            $text = 'Нажите на нопку и отправьте файл';

            $reply_markup = Telegram::replyKeyboardMarkup([ 
                'keyboard' => $keyboard, 
                'resize_keyboard' => true,
                'one_time_keyboard' => false,
            ]);

            Telegram::sendMessage([     
                'chat_id' => $this->update['callback_query']['message']['chat']['id'],
                'text' => $text,
                'reply_markup' => $reply_markup,
                'disable_web_page_preview' => true,
                'parse_mode' => 'Markdown'
            ]);
        }


    }

    public function closeTest(){
    	$state = $this->runner->state;

		if(!$this->condition){
			return false;
		}

       	$this->cr->status_id = 2;
       	$this->cr->test_status = $this->countPoints();
       	$this->cr->save();



        $done = 0;
        $next = null;
        foreach ($this->mr->condition_results as $key => $value) {
            if($value->status_id == 2){
                $done++;
            }
            if($value->status_id == 0){
               $next = $value;
               break;
            }            
        }

        if( $done == count($this->mr->condition_results) ){
            $this->mr->status_id = 2;
            $this->mr->save();

            $this->runner->profile->balance = 5000;
            $this->runner->profile->missions = $this->runner->profile->missions + 1;
            $this->runner->profile->save();
            $this->runner->state->state = 3;
            $this->runner->state->save();
        }

    	Telegram::sendMessage([     
    			'chat_id' => $this->update['callback_query']['message']['chat']['id'],
    			'text' => $this->resultsText(),
    			'parse_mode' => 'Markdown'
    	]);


        if($this->cr->condition->files){
            $params = [
                'chat_id' => $this->update['callback_query']['message']['chat']['id'],
            ];
            foreach ($this->cr->condition->files as $key => $file) {
                if($file->position == 1){
                    if($file->type == 0){
                        $params['photo'] = $file->file_id;
                        Telegram::sendPhoto($params);
                    }                   
                }
            }

        }

		if($this->cr->condition->end_text){
        	Telegram::sendMessage([     
    			'chat_id' => $this->update['callback_query']['message']['chat']['id'],
    			'text' => $this->cr->condition->end_text,
    			'parse_mode' => 'Markdown'
    		]);            
        }
        if($next){
            $os = new OpenService;
            $os->handle( $this->mr, $this->update, $this->runner );       
        }

    }

    public function resultsText()
    {
    	$test = Tests::where('condition_id', $this->cr->condition->id)->first();
    	$questions = json_decode( $test->body, true );
    	$answers = json_decode( $test->answers, true );
    	$text = '';
    	foreach ($questions as $key => $value) {
    		$text = $text.$value['q'].'
';
    		$text = $text.'*'.$value['a'][ $answers[$key] ].'*

';
    	}
    	return $text;
    }

    public function renewTest()
    {
    	$state = $this->runner->state;

		if(!$this->condition){
			return false;
		}


    	$this->cr->result = null;
    	$this->cr->test_status = 0;

    	$this->runner->state->state = 0;
    	$this->runner->state->save();

        $this->cr->save();

        $keyboard = [ ['Начать тест'] ];
        $text = 'Нажмите на кнопку "Начать тест", чтобы улучшить свой показатель. ';

        $reply_markup = Telegram::replyKeyboardMarkup([ 
        	'keyboard' => $keyboard, 
            'resize_keyboard' => true,
            'one_time_keyboard' => false,
        ]);

		Telegram::sendMessage([     
        	'chat_id' => $this->update['callback_query']['message']['chat']['id'],
            'text' => $text,
            'reply_markup' => $reply_markup,
            'parse_mode' => 'Markdown'
        ]);
    }
}
