<?php
namespace App\Services;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\Models\Runner;
use App\Models\RunnerState;
use App\Models\RunnerProfile;
use App\Models\Mission;
use App\Models\Condition;
use App\Models\MissionResult;
use App\Models\ConditionResult;
use Log;
use App\Services\TestService;
use App\Services\OpenService;
  
class CloseService
{
    public $update;
    public $runner;
    public $mission;
    public $condition;
    public $mr;
    public $cr;

	public function handle($cr,  $update)
    {

        $this->cr = $cr;
        $this->update = $update;
        $this->mr = $cr->mission_result;
        $this->runner = $cr->mission_result->runner;
        $this->mission = $cr->mission_result->mission;
        $this->condition = $cr->condition;

    	$this->closeConditionResult();
    }



    public function closeConditionResult()
    {
        $this->cr->update([
            'status_id' => 2,
        ]);



        if( isset( $this->update['message'] ) ){
            $chat_id =  $this->update['message']['chat']['id'];       
        }            

        if( isset( $this->update['callback_query'] ) ){
            $chat_id = $this->update['callback_query']['from']['id'];        
        }

        if($this->cr->condition->files){
            foreach ($this->cr->condition->files as $key => $file) {
                if($file->position == 2){
                    $this->sendFile($file, $chat_id);
                }
            }
        }


        if($this->cr->condition->end_text){
            Telegram::sendMessage([     
                'chat_id' => $chat_id,
                'text' => $this->cr->condition->end_text,
                'parse_mode' => 'Markdown'
            ]);            
        }

        if($this->cr->condition->files){
            foreach ($this->cr->condition->files as $key => $file) {

                if($file->position ==3){
                    $this->sendFile($file, $chat_id);
                }
            }
        }


        $this->runner->profile->points = $this->cr->condition->points_reward;
        $this->runner->profile->save();

        $done = 0;
        $next = null;
        foreach ($this->mr->condition_results as $key => $value) {
            if($value->status_id == 2){
                $done++;
            }
            if($value->status_id == 0){
               $next = $value;
               break;
            }            
        }

        if( $done == count($this->mr->condition_results) ){
            $this->mr->status_id = 2;
            $this->mr->save();

            $this->runner->profile->balance =  $this->runner->profile->balance + $this->mission->balance_reward;
            $this->runner->profile->missions = $this->runner->profile->missions + 1;
            $this->runner->profile->save();
            $this->runner->state->state = 3;
            $this->runner->state->save();
        } else {
            $os = new OpenService;
            $os->handle($this->mr, $this->update, $this->runner);
        }
    }

    public function sendFile($file, $chat_id){
        $params = [
            'chat_id' => $chat_id,
        ];

        if($file->type == 0){
            $params['photo'] = $file->file_id;
            Telegram::sendPhoto($params);
        }                       
        
        if($file->type == 1){
            $params['document'] = $file->file_id;
            Telegram::sendDocument($params);
        }  
    }

}