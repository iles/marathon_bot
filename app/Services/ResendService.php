<?php
namespace App\Services;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\Models\Runner;
use App\Models\RunnerState;
use App\Models\RunnerProfile;
use App\Models\Mission;
use App\Models\Condition;
use App\Models\MissionResult;
use App\Models\ConditionResult;
use Log;
use App\Services\TestService;
use App\Services\CloseService;

  
class ResendService
{
    public $update;
    public $cr;

	public function handle($cr, $update)
    {

        $this->update = $update;
        $this->cr = $cr;


    	$this->sendAnswer($cr);
    }



    public function sendAnswer($cr){

        $text = $cr->condition->start_text;

        if( $cr->condition->type_id == 1){
            $reply_markup = Telegram::replyKeyboardHide();  
        } elseif( $cr->condition->type_id == 2){
            
            $keyboard = [ ['Начать тест'] ];
            
            $reply_markup = Telegram::replyKeyboardMarkup([ 
                'keyboard' => $keyboard, 
                'resize_keyboard' => true,
                'one_time_keyboard' => false,
            ]);
        }elseif( $cr->condition->type_id == 3){
            
            $keyboard = [ ['Отправить файл'] ];
            
            $reply_markup = Telegram::replyKeyboardMarkup([ 
                'keyboard' => $keyboard, 
                'resize_keyboard' => true,
                'one_time_keyboard' => false,
            ]);
        }elseif( $cr->condition->type_id == 4){
            
            $keyboard = [ ['Отправить код'] ];
            
            $reply_markup = Telegram::replyKeyboardMarkup([ 
                'keyboard' => $keyboard, 
                'resize_keyboard' => true,
                'one_time_keyboard' => false,
            ]);
        }


        if( isset( $this->update['message'] ) ){
            $chat_id =  $this->update['message']['chat']['id'];       
        }            

        if( isset( $this->update['callback_query'] ) ){
            $chat_id = $this->update['callback_query']['from']['id'];        
        }

        
        if($cr->condition->files){
            foreach ($cr->condition->files as $key => $file) {
                if($file->position == 0){
                    $this->sendFile($file, $chat_id);
                }
            }
        }


        Telegram::sendMessage([     
            'chat_id' => $chat_id,
            'text' => $text,
            'parse_mode' => 'Html',
            'reply_markup' => $reply_markup,
        ]);

        
        if($cr->condition->files){
            foreach ($cr->condition->files as $key => $file) {
                if($file->position == 1){
                    $this->sendFile($file, $chat_id);
                }
            }
        }
    }



}
