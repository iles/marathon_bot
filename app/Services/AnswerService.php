<?php
namespace App\Services;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\Models\Runner;
use App\Models\RunnerState;
use App\Models\RunnerProfile;
use App\Models\Mission;
use App\Models\Condition;
use App\Models\MissionResult;
use App\Models\ConditionResult;
use Log;
use App\Services\TestService;
use App\Services\CloseService;
use App\Services\ResendService;

  
class AnswerService
{
    public $update;
    public $runner;
    public $mission;
    public $condition;
    public $mr;
    public $cr;

	public function handle($data)
    {

        $this->update = $data['update'];
        $this->runner = $data['runner'];
        $this->mission = $data['mission'];
        $this->condition = $data['condition'];
        $this->mr = $data['mr'];
        $this->cr = $data['cr'];

    	$this->handleState();
    }



    public function handleState()
    {
        if($this->cr->status_id == 1 && $this->cr->condition->type_id == 1){
            $this->cr->update([
                'result' => $this->update['message']['text'],
            ]);
            
            $close = new CloseService;
            $close->handle($this->cr, $this->update);             
        }elseif(  $this->cr->condition->type_id == 2){  

            $data['update'] = $this->update;
            $data['runner'] = $this->runner;
            $data['mission'] = $this->mission;
            $data['condition'] = $this->condition;
            $data['mr'] = $this->mr;
            $data['cr'] = $this->cr;

            $ts = new TestService;
            $ts->handle($data);
        }else{
            $rs = new ResendService;
            $rs->handle($this->cr, $this->update);
        }
    }



}
