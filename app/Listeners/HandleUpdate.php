<?php

namespace App\Listeners;

use App\Events\UpdateComing;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\Models\Runner;
use App\Models\RunnerState;
use App\Models\RunnerProfile;
use App\Models\Mission;
use App\Models\Condition;
use App\Models\MissionResult;
use App\Models\ConditionResult;


use Log;

class HandleUpdate
{
    public $runner;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateComing  $event
     * @return void
     */
    public function handle(UpdateComing $event)
    {

        $update = $event->update;
        Log::channel('updates')->info($update);
        

        if ( isset($update['callback_query']) ){            
            $runner = Runner::where('telegram_id', $update['callback_query']['from']['id'])->first(); 
        } else {
            $runner = Runner::where('telegram_id', $update['message']['from']['id'])->first();  
        }

        if( isset($update['message']['entities']) ){
            Telegram::commandsHandler(true);
            return false;
        }


        if(!$runner)
        {
            return false;
        }

        $this->runner = $runner;
        

        

        if($runner->profile->lifes == 0){
            $this->outOfLife();
            return false;
        }        

        if($runner->state->state == 3){
            $this->relax();
            return false;
        }

        $state = $runner->state;
        $data = [];
        $data['update'] = $update;
        $data['runner'] = $runner;
        
        $data['cr'] = ConditionResult::whereId($state->condition_result_id)->first();

        $data['mission'] = Mission::where('day_id', $state->mission_id)->first();
        $data['condition'] = Condition::where('id', $state->condition_id)->first();
        $data['mr'] = MissionResult::where('runner_id', $runner->id)->where('mission_id', $state->mission_id)->first();



        if ( isset($update['callback_query']) ){            
            $cdData = json_decode($update['callback_query']['data'], true);
            if( $cdData['type'] == 'test_results_confirm' ){
                $event->testService->handle($data);
                return false;

            }
        }


        if ( isset($update['callback_query']) ){            
            $cdData = json_decode($update['callback_query']['data'], true);
            if( $cdData['type'] == 'code_confirm' ){
                $event->codeService->handle($data);
                return false;
            }
        }


/*        if ( isset($update['message']['photo']) ){
            if($this->runner->state->state == 33 ) {
                $event->fileService->handle($data);
                return false;
            }
        }*/

        if($data['cr']->condition->type_id == 3 && $data['cr']->status_id == 1){
            if( isset($update['message']['photo']) ){
                $event->fileService->handle($data);
            } else {
                $this->send_file_wait();
                return;
            }
        }


        if ( isset($update['message']['text']) ){ 

            if($this->runner->state->state == 44 ){
                $event->codeService->handle($data);
                return false;
            }



            if($update['message']['text'] == 'Начать'){
                $event->beginService->handle($data);   
                return false;             
            } elseif($update['message']['text'] == 'Начать тест') {
                $event->testService->handle($data);
            } elseif($update['message']['text'] == 'Отправить файл') {
                $this->runner->state->state = 33;
                $this->runner->state->save();
                $this->send_file_wait();
                return false;
            } elseif($update['message']['text'] == 'Отправить код') {
                $this->runner->state->state = 44;
                $this->runner->state->save();
                $this->send_code_wait();
                return false;
            } else {
                $event->answerService->handle($data);
                return false;
            }
        }

    }

    public function send_file_wait()
    {
        Telegram::sendMessage([     
            'chat_id' => $this->runner->chat_id,
            'text' => 'Выберите файл и отправьте',
            'reply_markup' => Telegram::replyKeyboardHide(),        
            'parse_mode' => 'Markdown'
        ]);    
    }    

    public function send_code_wait()
    {
        Telegram::sendMessage([     
            'chat_id' => $this->runner->chat_id,
            'text' => 'Отправьте код',
            'reply_markup' => Telegram::replyKeyboardHide(),  
            'parse_mode' => 'Markdown'
        ]);    
    }


    public function outOfLife(){

        Telegram::sendMessage([     
            'chat_id' => $this->runner->chat_id,
            'text' => 'У вас закончились жизни, вы выбыли из марафона :(',
            'parse_mode' => 'Markdown'
        ]);     

    }    

    public function relax(){

        Telegram::sendMessage([     
            'chat_id' => $this->runner->chat_id,
            'text' => 'На сегодня все задания выполнены :)',
            'parse_mode' => 'Markdown'
        ]);     

    }
}
