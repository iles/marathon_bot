<?php

namespace App\Listeners;

use App\Events\RunnerCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\RunnerState;
use App\Models\RunnerProfile;


use App\Models\Mission;
use App\Models\MissionResult;
use App\Models\ConditionResult;

class SetRunnerState
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RunnerCreated  $event
     * @return void
     */
    public function handle(RunnerCreated $event)
    {

        
        $profile = new RunnerProfile([
            'runner_id' => $event->runner->id,
        ]);
        $event->runner->profile()->save($profile);

        $first = 0;

        $mission = Mission::where('day_id', 1)->first();
        $mission_result = MissionResult::create([
            'runner_id' => $event->runner->id,
            'mission_id' => $mission->id,
        ]);
        foreach ($mission->conditions as $key => $condition) {
            $condition_result = ConditionResult::create([
                'mission_result_id' => $mission_result->id,
                'condition_id' => $condition->id,
            ]);
            if($key == 0){
                $first = $condition_result->id;
            }
        }

        

        $state = new RunnerState([
            'runner_id' => $event->runner->id,
            'mission_id' => 1,
            'condition_id' => 1,
            'condition_result_id' => $first,
            'state' => 0,
        ]);

        $event->runner->state()->save($state);

    }
}
