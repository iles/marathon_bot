<?php

namespace App\Listeners;

use App\Events\UpdateComing;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class LogUpdate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateComing  $event
     * @return void
     */
    public function handle(UpdateComing $event)
    {
        Log::channel('updates')->info($event->update);
    }
}
