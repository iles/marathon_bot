<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Services\BeginService;
use App\Services\AnswerService;
use App\Services\TestService;
use App\Services\FileService;
use App\Services\CodeService;

class UpdateComing
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $update;
    public $beginService;
    public $answerService;
    public $testService;
    public $fileService;

    public function __construct($update)
    {
        $this->update = $update;
        $this->beginService = new BeginService();
        $this->answerService = new AnswerService;
        $this->testService = new TestService();
        $this->fileService = new FileService();
        $this->codeService = new CodeService();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
