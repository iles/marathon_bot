<?php

namespace App\Commands;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

use App\Models\Runner;



class HelpCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "help";

    /**
     * @var string Command Description
     */
    protected $description = "Помощь";

    /**
     * @inheritdoc
     */
    public function handle($arguments)
    {
        $commands = $this->telegram->getCommands();

        $text = '';
        
        foreach ($commands as $name => $handler) {
            $text .= sprintf('/%s - %s'.PHP_EOL, $name, $handler->getDescription());
        }


        
        $url_arr[0]['text']='Написать оператору'; 
        $url_arr[0]['url']='t.me/leilaqaratinvest';          

        $url_arr[1]['text']='Установить прокси'; 
        $url_arr[1]['url']='tg://proxy?server=bypass.guru&port=8843&secret=ddd06ecf975e9395d31e99721bc114967d';  


        $this->replyWithMessage([
            'text' => $text,
            'reply_markup'=> json_encode(['inline_keyboard'=>array($url_arr)]),
        ]);

    }
}
