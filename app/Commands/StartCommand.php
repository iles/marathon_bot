<?php

namespace App\Commands;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

use App\Models\Runner;
use App\Models\Result;

use Carbon\Carbon;

class StartCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "start";

    /**
     * @var string Command Description
     */
    protected $description = "Стартовая команда";

    /**
     * @inheritdoc
     */
    public function handle($arguments)
    {
        // This will send a message using `sendMessage` method behind the scenes to
        // the user/chat id who triggered this command.
        // `replyWith<Message|Photo|Audio|Video|Voice|Document|Sticker|Location|ChatAction>()` all the available methods are dynamically
        // handled when you replace `send<Method>` with `replyWith` and use the same parameters - except chat_id does NOT need to be included in the array.
        $update = $this->getUpdate();

        $runner = Runner::firstOrCreate([
            'telegram_id' =>  $update['message']['from']['id']
        ], [
            'telegram_id' =>  $update['message']['from']['id'],
            'is_bot' =>  $update['message']['from']['is_bot'],
            'chat_id' =>  $update['message']['chat']['id'],
            'name' => $update['message']['from']['first_name'],
            'username' => isset($update['message']['from']['username']) ? $update['message']['from']['username'] : '',
        ]);
        // This will update the chat status to typing...
        // $this->replyWithChatAction(['action' => Actions::TYPING]);

        //$emoticons = "\ud83d\udc4e";
        //$keyboard = [ ['Начать задание', json_decode('"'.$emoticons.'"') ] ];


        $keyboard = [ ['Начать'] ];

        $reply_markup = $this->telegram->replyKeyboardMarkup([ 
            'keyboard' => $keyboard, 
            'resize_keyboard' => true,
            'one_time_keyboard' => false,
        ]);




$text =



json_decode('"'.'\ud83d\udc4b'.'"').'Приветствую Вас в марафоне от компании «Qarat Invest» - «QARATHON»!

Итак, дорогой друг! Игра началась! Нужно привлечь за все время марафона больше 500 тысяч тенге и при этом, выполнив все задания, дойти до конца!

Самых активных также ждет вознаграждение
Тот, чья привлечённая сумма будет больше всех, получает *ДВОЙНОЙ КУШ!*

Старт - /start
Правила - /rules
Faq - /faq
Совет Дня - /sovet
Cтатистика  - /stat
Помощь - /help';

        //$text = 'Привет '.$update['message']['from']['first_name'].' !';

        //$this->replyWithMessage(['text' =>  $text, 'reply_markup' => $reply_markup]);
        
        // $url_arr[0]['text']='Начать первое задание'; 
        // $url_arr[0]['callback_data']='start_1';  
        // json_encode(['inline_keyboard'=>array($url_arr)])  


        $this->replyWithMessage([
            'text' => $text,
            'reply_markup'=> urldecode($reply_markup),
		'parse_mode'=>'Markdown',
        ]);







        // This will prepare a list of available commands and send the user.
        // First, Get an array of all registered commands
        // They'll be in 'command-name' => 'Command Handler Class' format.

        $commands = $this->getTelegram()->getCommands();

        // Build the list
        $response = '';
        foreach ($commands as $name => $command) {
            $response .= sprintf('/%s - %s' . PHP_EOL, $name, $command->getDescription());
        }

        // Reply with the commands list
        //$this->replyWithMessage(['text' => $response]);

        // Trigger another command dynamically from within this command
        // When you want to chain multiple commands within one or process the request further.
        // The method supports second parameter arguments which you can optionally pass, By default
        // it'll pass the same arguments that are received for this command originally.
        //$this->triggerCommand('subscribe');
    }
}
