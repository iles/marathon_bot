<?php

namespace App\Commands;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Log;

class StoreCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "store";

    /**
     * @var string Command Description
     */
    protected $description = "Start Command to get you started";

    /**
     * @inheritdoc
     */
    public function handle($arguments)
    {
        $update = $this->getUpdate();
        Log::debug( $update['message']['from']);
        //$name = $update->getMessage()->from->firstName;
        $text = "Hello, www.";
        $this->replyWithMessage(['text' => $text]);
    }
}