<?php

namespace App\Commands;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

use App\Models\Advice;
use Log;

class SovetCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "sovet";

    /**
     * @var string Command Description
     */
    protected $description = "Совет дня";

    /**
     * @inheritdoc
     */
    public function handle($arguments)
    {
        $text = Advice::whereDate('date', '=', date('Y-m-d'))->first();
        if(!$text){
            return false;
        }
        $this->replyWithMessage(['text' => $text->body, 'parse_mode' => 'HTML']);
    }
}