<?php

namespace App\Commands;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

use App\Models\Runner;
use App\Models\Result;



class StatCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "stat";

    /**
     * @var string Command Description
     */
    protected $description = "Моя статистика";

    /**
     * @inheritdoc
     */
    public function handle($arguments)
    {

        $update = $this->getUpdate();

        $telegram_id = $update['message']['from']['id'];

        $runner = Runner::where('telegram_id', $telegram_id)->first();


        if(!$runner){
            return false;
        }
        if(!$runner->profile){
            return false;
        }


/*        $text = 'Имя - *'.$runner->name.'* 

        Жизней - *'.$runner->profile->lifes.'*

        Баланс - *'.$runner->profile->balance.' тг*

        Очки - *'.$runner->profile->points.'*

        Заданий выполнено  - *'.$runner->profile->missions.'*';*/
        $text = 'Имя - <b>'.$runner->name.'</b> 

Жизней - <b>'.$runner->profile->lifes.'</b>

Баланс - <b>'.$runner->profile->balance.' тг</b>

Баллов - <b>'.$runner->profile->points.'</b>

Заданий выполнено  - <b>'.$runner->profile->missions.'</b>';
        $this->replyWithMessage([
            'text' =>  $text, 
            'parse_mode' => 'Html'
        ]);

    }
}