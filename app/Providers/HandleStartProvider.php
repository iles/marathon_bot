<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\BeginService;

class HandleStartProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\BeginService', function ($app) {
          return new BeginService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
