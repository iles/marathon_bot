<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Events\ProfileUpdated;

class RunnerProfile extends Model
{

	protected $fillable = [
        'runner_id',
    ];

    public function runner()
    {
        return $this->belongsTo('App\Models\Runner');
    }

    protected $dispatchesEvents = [
        'updated' =>  ProfileUpdated::class,
    ];
}
