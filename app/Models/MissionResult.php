<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MissionResult extends Model
{
	protected $fillable = [
        'runner_id', 'mission_id'
    ];

    public function runner()
    {
        return $this->belongsTo('App\Models\Runner');
    }    

    public function mission()
    {
        return $this->belongsTo('App\Models\Mission');
    }    

    public function condition_results()
    {
        return $this->hasMany('App\Models\ConditionResult');
    }
}
