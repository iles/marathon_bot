<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RunnerState extends Model
{
	protected $fillable = [
        'runner_id', 'mission_id', 'condition_id', 'state', 'condition_result_id',
    ];

    public function runner()
    {
        return $this->belongsTo('App\Models\Runner');
    }
}
