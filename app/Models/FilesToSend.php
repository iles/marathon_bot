<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FilesToSend extends Model
{
    public function mission()
    {
        return $this->belongsTo('App\Models\Mission');
    }      

    public function condition()
    {
        return $this->belongsTo('App\Models\Condition');
    }  
}
