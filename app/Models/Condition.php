<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Condition extends Model
{
	protected $fillable = [
        'start_text',
    ];

    public function mission()
    {
        return $this->belongsTo('App\Models\Mission');
    }

    public function results()
    {
        return $this->hasMany('App\Models\ConditionResult');
    }

    public function files()
    {
        return $this->hasMany('App\Models\FilesToSend');
    }
}
