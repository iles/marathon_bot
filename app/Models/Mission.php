<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mission extends Model
{
	
	public function conditions()
    {
        return $this->hasMany('App\Models\Condition');
    }	

    public function results()
    {
        return $this->hasMany('App\Models\MissionResult');
    }    

    public function files()
    {
        return $this->hasMany('App\Models\FilesToSend');
    }
}
