<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Events\RunnerCreated;

class Runner extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'telegram_id', 'is_bot', 'lastname' , 'chat_id',
    ];

    public function profile()
    {
        return $this->hasOne('App\Models\RunnerProfile');
    }    

    public function state()
    {
        return $this->hasOne('App\Models\RunnerState');
    }

    public function mission_results()
    {
        return $this->hasMany('App\Models\MissionResult');
    }    


    protected $dispatchesEvents = [
        'created' => RunnerCreated::class,
    ];


}