<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConditionResult extends Model
{
	protected $fillable = [
        'mission_result_id', 'condition_id', 'status_id', 'result',
    ];

    public function condition()
    {
        return $this->belongsTo('App\Models\Condition');
    }     

    public function mission_result()
    {
        return $this->belongsTo('App\Models\MissionResult');
    }  
}