<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conditions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mission_id')->unsigned();
            $table->integer('type_id');
            $table->integer('points_reward')->nullable();
            $table->integer('files_total')->nullable();
            $table->mediumtext('start_text')->nullable();
            $table->mediumtext('end_text')->nullable();
            $table->timestamps();
            $table->foreign('mission_id')
                  ->references('id')->on('missions')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conditions');
    }
}
