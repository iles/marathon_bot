<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRunnerProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('runner_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('runner_id')->unsigned();
            $table->integer('lifes')->default(3);
            $table->integer('balance')->default(0);
            $table->integer('points')->default(0);
            $table->integer('missions')->default(0);
            $table->timestamps();
            $table->foreign('runner_id')
                  ->references('id')->on('runners')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('runner_profiles');
    }
}
