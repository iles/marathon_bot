<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConditionResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('condition_results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mission_result_id')->unsigned();
            $table->integer('condition_id')->unsigned();
            $table->integer('status_id')->default(0);
            $table->mediumtext('result')->nullable();
            $table->integer('test_status')->nullable();
            $table->integer('files_count')->nullable();
            $table->string('code')->nullable();
            $table->timestamps();
            $table->foreign('mission_result_id')
                  ->references('id')->on('mission_results')
                  ->onDelete('cascade');             
            $table->foreign('condition_id')
                  ->references('id')->on('conditions')
                  ->onDelete('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('condition_results');
    }
}
