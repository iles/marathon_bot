<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesToSendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files_to_sends', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mission_id')->unsigned()->nullable();
            $table->integer('condition_id')->unsigned()->nullable();
            $table->integer('position')->default(0);
            $table->integer('type')->default(0);
            $table->string('file_id');
            $table->timestamps();
            $table->foreign('mission_id')
                  ->references('id')->on('missions')
                  ->onDelete('cascade');            
            $table->foreign('condition_id')
                  ->references('id')->on('conditions')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files_to_sends');
    }
}
