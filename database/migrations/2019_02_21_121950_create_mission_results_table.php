<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMissionResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mission_results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('runner_id')->unsigned();
            $table->integer('mission_id')->unsigned();
            $table->integer('status_id')->default(0);
            $table->string('result')->nullable();
            $table->timestamps();
            $table->foreign('runner_id')
                  ->references('id')->on('runners')
                  ->onDelete('cascade');            
            $table->foreign('mission_id')
                  ->references('id')->on('missions')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mission_results');
    }
}
