<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRunnerStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('runner_states', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('runner_id')->unsigned();
            $table->integer('mission_id')->default(0);
            $table->integer('condition_id')->default(0);
            $table->integer('condition_result_id')->default(0);
            $table->integer('state')->nullable();
            $table->timestamps();
            $table->foreign('runner_id')
                  ->references('id')->on('runners')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('runner_states');
    }
}
