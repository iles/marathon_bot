<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
//        $this->call(RunnersTableSeeder::class);
//        $this->call(RunnerStatesTableSeeder::class);
//        $this->call(RunnerProfilesTableSeeder::class);
        $this->call(MarathonsTableSeeder::class);
        $this->call(MissionsTableSeeder::class);
        $this->call(ConditionsTableSeeder::class);
        $this->call(FilesToSendsTableSeeder::class);
        $this->call(TestsTableSeeder::class);
//        $this->call(MissionResultsTableSeeder::class);
//        $this->call(ConditionResultsTableSeeder::class);
        $this->call(AdvicesTableSeeder::class);
    }
}
