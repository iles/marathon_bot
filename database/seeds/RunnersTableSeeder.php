<?php

use Illuminate\Database\Seeder;

class RunnersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('runners')->delete();
        
        \DB::table('runners')->insert(array (
            0 => 
            array (
                'id' => 1,
                'telegram_id' => 51285582,
                'chat_id' => 51285582,
                'is_bot' => '0',
                'name' => 'TURBO',
                'lastname' => NULL,
                'username' => 'Turbo5371',
                'created_at' => '2019-03-02 21:17:52',
                'updated_at' => '2019-03-02 21:17:52',
            ),
        ));
        
        
    }
}