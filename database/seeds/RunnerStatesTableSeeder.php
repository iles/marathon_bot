<?php

use Illuminate\Database\Seeder;

class RunnerStatesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('runner_states')->delete();
        
        \DB::table('runner_states')->insert(array (
            0 => 
            array (
                'id' => 1,
                'runner_id' => 1,
                'mission_id' => 2,
                'condition_id' => 4,
                'condition_result_id' => 4,
                'state' => 3,
                'created_at' => '2019-03-02 21:17:52',
                'updated_at' => '2019-03-03 17:47:58',
            ),
        ));
        
        
    }
}