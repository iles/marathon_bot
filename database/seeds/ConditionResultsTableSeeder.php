<?php

use Illuminate\Database\Seeder;

class ConditionResultsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('condition_results')->delete();
        
        \DB::table('condition_results')->insert(array (
            0 => 
            array (
                'id' => 1,
                'mission_result_id' => 1,
                'condition_id' => 1,
                'status_id' => 2,
                'result' => 'asdasd',
                'test_status' => NULL,
                'files_count' => NULL,
                'code' => NULL,
                'created_at' => '2019-03-02 21:17:52',
                'updated_at' => '2019-03-02 21:18:01',
            ),
            1 => 
            array (
                'id' => 2,
                'mission_result_id' => 1,
                'condition_id' => 2,
                'status_id' => 2,
                'result' => '{"1":"В","2":"Б","3":"А","4":"В","5":"Б","6":"Б","7":"А","8":"А","9":"Б","10":"Б"}',
                'test_status' => 10,
                'files_count' => NULL,
                'code' => NULL,
                'created_at' => '2019-03-02 21:17:52',
                'updated_at' => '2019-03-02 21:32:24',
            ),
            2 => 
            array (
                'id' => 3,
                'mission_result_id' => 2,
                'condition_id' => 3,
                'status_id' => 2,
                'result' => '{"1":"Б","2":"Б","3":"В","4":"Б","5":"Б","6":"Б","7":"Б","8":"Б","9":"Б","10":"Б"}',
                'test_status' => 4,
                'files_count' => NULL,
                'code' => NULL,
                'created_at' => '2019-03-02 21:42:09',
                'updated_at' => '2019-03-03 17:47:41',
            ),
            3 => 
            array (
                'id' => 4,
                'mission_result_id' => 2,
                'condition_id' => 4,
                'status_id' => 2,
                'result' => ';photos/file_57.jpg;photos/file_58.jpg;photos/file_59.jpg;photos/file_60.jpg',
                'test_status' => NULL,
                'files_count' => 1,
                'code' => NULL,
                'created_at' => '2019-03-02 21:42:09',
                'updated_at' => '2019-03-03 17:47:58',
            ),
        ));
        
        
    }
}