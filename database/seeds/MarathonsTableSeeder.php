<?php

use Illuminate\Database\Seeder;

class MarathonsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('marathons')->delete();
        
        \DB::table('marathons')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Каратон',
                'user_id' => 1,
                'status_id' => 1,
                'start_date' => '2019-02-21',
                'end_date' => '2019-02-28',
                'created_at' => '2019-02-21 15:32:18',
                'updated_at' => '2019-02-21 15:32:18',
            ),
        ));
        
        
    }
}