<?php

use Illuminate\Database\Seeder;

class MissionResultsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('mission_results')->delete();
        
        \DB::table('mission_results')->insert(array (
            0 => 
            array (
                'id' => 1,
                'runner_id' => 1,
                'mission_id' => 1,
                'status_id' => 2,
                'result' => NULL,
                'created_at' => '2019-03-02 21:17:52',
                'updated_at' => '2019-03-02 21:32:24',
            ),
            1 => 
            array (
                'id' => 2,
                'runner_id' => 1,
                'mission_id' => 2,
                'status_id' => 2,
                'result' => NULL,
                'created_at' => '2019-03-02 21:42:09',
                'updated_at' => '2019-03-03 17:47:58',
            ),
        ));
        
        
    }
}