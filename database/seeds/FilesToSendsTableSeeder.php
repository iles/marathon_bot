<?php

use Illuminate\Database\Seeder;

class FilesToSendsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('files_to_sends')->delete();
        
        \DB::table('files_to_sends')->insert(array (
            0 => 
            array (
                'id' => 1,
                'mission_id' => NULL,
                'condition_id' => 1,
                'position' => 1,
                'type' => 2,
                'file_id' => 'BAADAgADIQMAAmsi6Evp5Nv7kyyY8wI',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'mission_id' => NULL,
                'condition_id' => 3,
                'position' => 1,
                'type' => 1,
                'file_id' => 'BQADAgADUwMAApw_2EtfTyMtipyd_gI',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'mission_id' => NULL,
                'condition_id' => 5,
                'position' => 1,
                'type' => 1,
                'file_id' => 'BQADAgADUwMAApw_2EtfTyMtipyd_gI',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}