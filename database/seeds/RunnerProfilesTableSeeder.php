<?php

use Illuminate\Database\Seeder;

class RunnerProfilesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('runner_profiles')->delete();
        
        \DB::table('runner_profiles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'runner_id' => 1,
                'lifes' => 3,
                'balance' => 5000,
                'points' => 50,
                'missions' => 1,
                'created_at' => '2019-03-02 21:17:52',
                'updated_at' => '2019-03-02 21:32:24',
            ),
        ));
        
        
    }
}