<?php

use Illuminate\Database\Seeder;

class AdvicesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('advices')->delete();
        
        \DB::table('advices')->insert(array (
            0 => 
            array (
            'body' => 'Следуйте за своей мечтой и ставьте большие цели. Неуверенный же в себе человек гонится за мелкими целями.&#10; А Ваши цели должны быть крупными!&#10;&#10;Честно ответьте одним сообщением на вопрос, посмотрите видео, пройдите тест)',
            'created_at' => NULL,
            'date' => '2019-02-25',
            'day_id' => 1,
            'id' => 1,
            'updated_at' => NULL,
        ),
        1 => 
        array (
            'body' => 'При падении всегда поднимайтесь. Не стоит опускать руки после маленькой или даже крупной неудачи.',
            'created_at' => NULL,
            'date' => '2029-12-21',
            'day_id' => 2,
            'id' => 2,
            'updated_at' => NULL,
        ),
        2 => 
        array (
            'body' => 'Расширяйте горизонты! Действуйте. Всегда занимайтесь саморазвитием. Только неудачники откладывают все',
            'created_at' => NULL,
            'date' => '2029-12-22',
            'day_id' => 3,
            'id' => 3,
            'updated_at' => NULL,
        ),
    ));
        
        
    }
}